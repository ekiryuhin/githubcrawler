package com.ekiryuhin.webcrawler.domains.Github;

import com.ekiryuhin.webcrawler.domains.Profile;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GithubProfile extends Profile {

    @SerializedName(value = "login")
    private String username;
    @SerializedName(value = "name")
    private String fullname;
    @SerializedName(value = "company")
    private String company;
    @SerializedName(value = "location")
    private String location;

    private List<GithubRepo> repos;
    private GithubRepo mainRepo;
    private String mainLanguage;

    public GithubProfile(String username, String fullname, String company, String location) {
        this.username = username;
        this.fullname = fullname;
        this.company = company;
        this.location = location;
    }

    public GithubProfile(String username, String fullname, String company, String location, List<GithubRepo> repos) {
        this.username = username;
        this.fullname = fullname;
        this.company = company;
        this.location = location;
        this.repos = repos;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<GithubRepo> getRepos() {
        return repos;
    }

    public void setRepos(List<GithubRepo> repos) {
        this.repos = repos;
    }

    public void setMainRepo(GithubRepo mainRepo) {
        this.mainRepo = mainRepo;
    }

    public String getMainLanguage() {
        if (this.mainLanguage != null) return this.mainLanguage;
        else {
            Map<String, Integer> languages = new HashMap<>();
            Map.Entry<String, Integer> mainLanguage = null;

            for (GithubRepo repo : repos)
                if (languages.containsKey(repo.getLanguage()))
                    languages.put(repo.getLanguage(), languages.get(repo.getLanguage()) + 1);
                else languages.put(repo.getLanguage(), 1);

            for (Map.Entry<String, Integer> language : languages.entrySet())
                if (mainLanguage == null || language.getValue() < mainLanguage.getValue())
                    mainLanguage = language;
            return (this.mainLanguage = mainLanguage.getKey());
        }
    }

    public GithubRepo getMainRepo() {
        return mainRepo == null ? mainRepo = Collections.max(repos, (o1, o2) -> o1.getWatchers() >= o2.getWatchers() ? 1 : -1) : mainRepo;
    }

    public String getMainRepoName() {
        return getMainRepo().getName();
    }

    public Integer getMainRepoStars() {
        return getMainRepo().getStars();
    }

    public String getMainRepoLanguage() {
        return getMainRepo().getLanguage();
    }

    @Override
    public String toString() {
        return "GithubProfile{" +
                "username='" + username + '\'' +
                ", fullname='" + fullname + '\'' +
                ", company='" + company + '\'' +
                ", location='" + location + '\'' +
                ", repos=" + repos +
                '}';
    }
}
