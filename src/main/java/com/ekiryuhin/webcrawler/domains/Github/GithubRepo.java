package com.ekiryuhin.webcrawler.domains.Github;

import com.google.gson.annotations.SerializedName;

public class GithubRepo {
    @SerializedName(value = "id")
    private long id;
    @SerializedName(value = "name")
    private String name;
    @SerializedName(value = "url")
    private String URL;
    @SerializedName(value = "description")
    private String description;
    @SerializedName(value = "language")
    private String language;
    @SerializedName(value = "watchers_count")
    private int watchers;
    @SerializedName(value = "stargazers_count")
    private int stars;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getWatchers() {
        return watchers;
    }

    public void setWatchers(int watchers) {
        this.watchers = watchers;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", URL='" + URL + '\'' +
                ", description='" + description + '\'' +
                ", language='" + language + '\'' +
                ", watchers=" + watchers +
                ", stars=" + stars;
    }
}
