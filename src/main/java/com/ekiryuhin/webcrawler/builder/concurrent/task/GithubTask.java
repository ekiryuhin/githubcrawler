package com.ekiryuhin.webcrawler.builder.concurrent.task;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.concurrent.Callable;

public abstract class GithubTask<T> implements Callable<T> {
    private final URL url;

    public GithubTask(URL url) {
        this.url = url;
    }

    private String inputStreamToString(InputStream is) {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    protected String getAnswer() throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream in = connection.getInputStream();
        String answer = inputStreamToString(in);
        in.close();
        connection.disconnect();
        return answer;
    }

}
