package com.ekiryuhin.webcrawler.builder.concurrent.task;

import com.ekiryuhin.webcrawler.builder.parser.GithubRepoParser;
import com.ekiryuhin.webcrawler.domains.Github.GithubRepo;

import java.net.URL;
import java.util.List;

public class GetReposTask extends GithubTask<List<GithubRepo>> {

    public GetReposTask(URL url) {
        super(url);
    }

    @Override
    public List<GithubRepo> call() throws Exception {
        return new GithubRepoParser().parse(getAnswer());
    }

}
