package com.ekiryuhin.webcrawler.builder.concurrent.task;

import com.ekiryuhin.webcrawler.builder.parser.GithubProfileParser;
import com.ekiryuhin.webcrawler.domains.Github.GithubProfile;

import java.net.URL;

public class GetProfileTask<T extends GithubProfile> extends GithubTask<GithubProfile> {

    public GetProfileTask(URL url) {
        super(url);
    }

    @Override
    public GithubProfile call() throws Exception {
        return new GithubProfileParser().parse(getAnswer());
    }
}
