package com.ekiryuhin.webcrawler.builder.concurrent;

import com.ekiryuhin.webcrawler.domains.Profile;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class ProfileManager implements ThreadManager<Profile> {

    private final int MAX_PARALLEL_REQUESTS;
    private final int REQUEST_DELAY;
    private final ScheduledExecutorService pool;
    private final List<ScheduledFuture<Profile>> profileFutures = new LinkedList<>();

    public ProfileManager(int MAX_PARALLEL_REQUESTS, int REQUEST_DELAY) {
        this.MAX_PARALLEL_REQUESTS = MAX_PARALLEL_REQUESTS;
        this.REQUEST_DELAY = REQUEST_DELAY;
        this.pool = Executors.newScheduledThreadPool(MAX_PARALLEL_REQUESTS);
    }

    @Override
    public ScheduledFuture<Profile> execute(Callable<Profile> callable) {
        ScheduledFuture<Profile> future = pool.schedule(callable, REQUEST_DELAY, TimeUnit.MILLISECONDS);
        profileFutures.add(future);
        return future;
    }

    @Override
    public List<Profile> getAll() throws ExecutionException, InterruptedException {
        pool.shutdown();
        LinkedList<Profile> profiles = new LinkedList<>();
        for (ScheduledFuture<Profile> profileFuture : profileFutures)
            profiles.add(profileFuture.get());

        return profiles;
    }
}
