package com.ekiryuhin.webcrawler.builder.concurrent;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ScheduledFuture;

public interface ThreadManager<T> {
   ScheduledFuture<T> execute(Callable<T> callable);
   List<T> getAll() throws ExecutionException, InterruptedException;
}
