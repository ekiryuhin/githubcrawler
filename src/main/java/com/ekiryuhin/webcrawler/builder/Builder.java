package com.ekiryuhin.webcrawler.builder;

import com.ekiryuhin.webcrawler.builder.concurrent.ThreadManager;
import com.ekiryuhin.webcrawler.builder.concurrent.task.GetProfileTask;
import com.ekiryuhin.webcrawler.builder.file.FileParser;
import com.ekiryuhin.webcrawler.domains.Profile;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Builder<T extends Profile> {

    private final File source;
    private final ThreadManager<T> threadManager;
    private final FileParser fileParser;

    public Builder(File source, ThreadManager threadManager, FileParser fileParser) {
        this.source = source;
        this.threadManager = threadManager;
        this.fileParser = fileParser;
    }

    public List<T> build() throws IOException, URISyntaxException, ExecutionException, InterruptedException {
        for (URL url : fileParser.parse(source))
            threadManager.execute(new GetProfileTask(url));
        return threadManager.getAll();
    }

}
