package com.ekiryuhin.webcrawler.builder.parser;

public interface Parser<T> {
    T parse(String content) throws Exception;
}
