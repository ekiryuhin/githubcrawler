package com.ekiryuhin.webcrawler.builder.parser;

import com.ekiryuhin.webcrawler.builder.concurrent.task.GetReposTask;
import com.ekiryuhin.webcrawler.domains.Github.GithubProfile;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.net.URL;

public class GithubProfileParser implements Parser<GithubProfile> {

    private final Gson gson = new Gson();
    @Override
    public GithubProfile parse(String content) throws Exception {
        JsonObject profileJSONObject = gson.fromJson(content, JsonObject.class);
        GithubProfile profile = gson.fromJson(content, GithubProfile.class);

        URL repos_url = new URL(profileJSONObject.get("repos_url").toString().replaceAll("\"", ""));
        profile.setRepos(new GetReposTask(repos_url).call());

        return profile;
    }
}
