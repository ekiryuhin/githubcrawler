package com.ekiryuhin.webcrawler.builder.parser;

import com.ekiryuhin.webcrawler.domains.Github.GithubRepo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class GithubRepoParser implements Parser<List<GithubRepo>> {

    private final Gson gson = new Gson();
    @Override
    public List<GithubRepo> parse(String content) {
            return gson.fromJson(content, new TypeToken<ArrayList<GithubRepo>>(){}.getType());
    }
}
