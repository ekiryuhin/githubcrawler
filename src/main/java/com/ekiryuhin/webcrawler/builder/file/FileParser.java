package com.ekiryuhin.webcrawler.builder.file;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Set;

public interface FileParser {
        Set<URL> parse(File file) throws IOException;
}
