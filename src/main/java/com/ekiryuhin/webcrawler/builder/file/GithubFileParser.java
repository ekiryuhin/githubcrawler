package com.ekiryuhin.webcrawler.builder.file;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GithubFileParser implements FileParser {

    private final String REQUEST = "https://api.github.com/users%s";

    @Override
    public Set<URL> parse(File file) throws IOException {
        return prepareURLs(Files.readAllLines(Paths.get(file.getPath())));
    }

    private Set<URL> prepareURLs (List<String> content) throws MalformedURLException {

        HashSet<URL> urls = new HashSet<>();
        for (String url : content)
            urls.add(new URL(String.format(REQUEST, new URL(url).getPath())));

        return urls;
    }

}
