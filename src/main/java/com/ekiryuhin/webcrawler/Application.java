package com.ekiryuhin.webcrawler;

import com.ekiryuhin.webcrawler.builder.Builder;
import com.ekiryuhin.webcrawler.builder.concurrent.ProfileManager;
import com.ekiryuhin.webcrawler.builder.file.GithubFileParser;
import com.ekiryuhin.webcrawler.domains.Github.GithubProfile;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class Application {

    public static void main(String[] args) {

        Map<String, String> envVars = System.getenv();
        String maxParallelRequests = envVars.get("crawler.maxParallelRequests");
        String requestDelayMs = envVars.get("crawler.requestDelayMs");
        File source;

        if (args.length == 0)
            System.out.println("Enter filename");
        else {
            source = new File(args[0]);
            if (!checkFile(source))
                System.out.println("File " + source.getPath() + " doesn't exist");
            else {
                int threadLimit = 1;
                int delay = 500;

                if (checkMaxParallelRequestsLimit(maxParallelRequests))
                    threadLimit = Integer.parseInt(maxParallelRequests);
                if (checkRequestDelay(requestDelayMs))
                    delay = Integer.parseInt(requestDelayMs);

                try {
                    for (GithubProfile profile : new Builder<GithubProfile>(source, new ProfileManager(threadLimit, delay), new GithubFileParser()).build())
                        System.out.println(
                                "Username: " + profile.getUsername() + "\n" +
                                        "Full name: " + profile.getFullname() + "\n" +
                                        "Company: " + profile.getCompany() + "\n" +
                                        "Location: " +profile.getLocation() + "\n" +
                                        "Main language: " + profile.getMainLanguage() + "\n" +
                                        "Main repo: " + profile.getMainRepo() + "\n" +
                                        "Main repo rating: " +  profile.getMainRepoStars() + "\n" +
                                        "Main repo language: " + profile.getMainRepoLanguage() + "\n"
                        );
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (URISyntaxException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    private static boolean checkFile(File file) {
        return file.exists();
    }

    private static boolean checkMaxParallelRequestsLimit(String limit) {
        try {
            return Integer.parseInt(limit) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private static boolean checkRequestDelay(String delay) {
        try {
            return Integer.parseInt(delay) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

}
